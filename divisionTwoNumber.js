const divideTwoNumber = (dividend, divisor) => {
  let count = 0;
  let result = dividend
  while (result > 0) {
    result = result - divisor;
    if (result >= 0) {
      ++count;
    }
  }
  return count;
};

module.exports = divideTwoNumber;

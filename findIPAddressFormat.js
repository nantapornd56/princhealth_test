const findIPAddressFormat = (input) => {
  let splitInput = input.split(".");
  let result = false;
  if (splitInput.length == 4) {
    if (splitInput.indexOf("") == 1) {
      return false;
    } else {
      for (let i = 0; i < splitInput.length; i++) {
        if (parseInt(splitInput[i]) >= 0 || parseInt(splitInput[i]) <= 255) {
          result = true;
        } else {
          return false;
        }

        if (checkIsValidateSubString(splitInput[i]) == false) {
          return false;
        }
      }
    }
  } else return false;
  return result;
};

const checkIsValidateSubString = (input) => {
  for (let i = 0; i < input.length; i++) {
    if (isNaN(input[i])) {
      return false;
    }
    if (input.length > 1 && input[i] == 0 && i == 0) {
      return false;
    }
  }
};

module.exports = findIPAddressFormat;

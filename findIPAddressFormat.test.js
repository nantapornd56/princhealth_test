const findIPAddressFormat = require('./findIPAddressFormat');

describe('Test findIPAddressFormat',()=>{
    it('Should return true when 192.168.1.1',()=>{
        expect(findIPAddressFormat('192.168.1.1')).toBe(true);
    })
    it('Should return true when 0.0.0.0',()=>{
        expect(findIPAddressFormat('0.0.0.0')).toBe(true);
    })
    it('Should return false when 192.168.1',()=>{
        expect(findIPAddressFormat('192.168.1')).toBe(false);
    })
    it('Should return false when 192..168.1',()=>{
        expect(findIPAddressFormat('192..168.1')).toBe(false);
    })
    it('Should return false when 192.168.X.1',()=>{
        expect(findIPAddressFormat('192.168.X.1')).toBe(false);
    })
    it('Should return false when 12.34.5.oops',()=>{
        expect(findIPAddressFormat('12.34.5.oops')).toBe(false);
    })
    it('Should return false when ""',()=>{
        expect(findIPAddressFormat('')).toBe(false);
    })
    it('Should return false when 12.34.0x5.6',()=>{
        expect(findIPAddressFormat('12.34.0x5.6')).toBe(false);
    })
    it('Should return false when 12.34.5x.6',()=>{
        expect(findIPAddressFormat('12.34.5x.6')).toBe(false);
    })
    it('Should return false when 12.34.05.6',()=>{
        expect(findIPAddressFormat('12.34.05.6')).toBe(false);
    })
    
})
    
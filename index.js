const express = require('express');
const findBracket = require('./findBracket')
const divideTwoNumber = require('./divisionTwoNumber')
const findIPAddressFormat = require('./findIPAddressFormat')
const findRomanNumber = require('./findRomanNumber');

const app = express();
const port = 3387;

app.use(express.json())

app.get('/bracket/:input',(req,res)=>{
    try{
        res.json(findBracket(req.params.input))
    }
    catch(err){
        res.json(new Error(err))
    }
})

app.post('/division',(req,res)=>{
    try{
        const {dividend, divisor} = req.body
        res.json(divideTwoNumber(dividend,divisor))
    }
    catch(err){
        res.json(new Error(err))
    }
})

app.get('/ipaddress/:input',(req,res)=>{
    try{

        res.json(findIPAddressFormat(req.params.input))
    }
    catch(err){
        res.json(new Error(err))
    }
})

app.get('/roman/:input',(req,res)=>{
    try{

        res.json(findRomanNumber(req.params.input))
    }
    catch(err){
        res.json(new Error(err))
    }
})

app.listen(port,()=>{
    console.log(`Server listen on http://localhost:${port}`)
})
# How to run project

1. run command
    - npm install
2. run command to start server
    - npm run start-dev

### How to validate result for each problem after start server success

1. Problem 1
    -   curl --location -g --request GET `http://localhost:3387/bracket/${your_input_here}` \ --data-raw ''


### example

    -   curl --location -g --request GET 'http://localhost:3387/bracket/{}' \
        --data-raw ''

2. Problem 2
    -   curl --location --request POST 'http://localhost:3387/division' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "dividend": `${your_input_here}`,
            "divisor" : `${your_input_here}`
        }'

 ### example

    - curl --location --request POST 'http://localhost:3387/division' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "dividend": 4,
            "divisor" : 2
        }'

3. Problem 3
    -   curl --location --request GET `http://localhost:3387/ipaddress/${your_input_here}` \ --data-raw ''

### example
    - curl --location --request GET 'http://localhost:3387/ipaddress/10.157.2.3' \ --data-raw ''


4. Problem 4
    -   curl --location --request GET `'http://localhost:3387/roman/${your_input_here}'` \ --data-raw ''

### example
    - curl --location --request GET 'http://localhost:3387/roman/10' \ --data-raw ''

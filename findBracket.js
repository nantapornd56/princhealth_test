//find bracket

const findBracket = (stringInput) => {
  let dataArray = [];

  if (stringInput.length <= 1) {
    return false;
  } else {
    for (let i = 0; i < stringInput.length; i++) {
      let currentChar = stringInput[i];
      if (currentChar == "(" || currentChar == "{" || currentChar == "[") {
        dataArray.push(currentChar);
      } else if (
        currentChar == ")" ||
        currentChar == "}" ||
        currentChar == "]"
      ) {
        let check = dataArray.pop();
        if (check != "(" && currentChar == ")") {
          return false;
        } else if (check != "{" && currentChar == "}") {
          return false;
        } else if (check != "[" && currentChar == "]") {
          return false;
        }
      }
      else{
        return false;
      }
    }
    return dataArray.length ? false : true;
  }
};

module.exports = findBracket;

const findBracket = require('./findBracket');

describe('Test find Bracket',()=>{
    it('Should return false when } input',()=>{
        expect(findBracket("}")).toBe(false)
    })

    it('Should return false when {]}',()=>{
        expect(findBracket("{]}")).toBe(false)
    })

    it('Should return false when 11223',()=>{
        expect(findBracket("11223")).toBe(false)
    })

    it('Should return true when ()',()=>{
        expect(findBracket("()")).toBe(true);
    })

    it('Should return true when {[]}',()=>{
        expect(findBracket("{[]}")).toBe(true);
    })
    
})
const divideTwoNumber = require('./divisionTwoNumber')

describe('Test divideTwoNumber',()=>{
    it('Should return 0 when dividend less than divisor',()=>{
        expect(divideTwoNumber(1,4)).toBe(0)
    })
    it('Should return 1 when dividend and divisor are equal',()=>{
        expect(divideTwoNumber(4,4)).toBe(1)
    })
    it('Should return count when dividend more than divisor',()=>{
        expect(divideTwoNumber(16,2)).toBe(8)
    })
    it('Should return 0 when dividend is 0',()=>{
        expect(divideTwoNumber(0,2)).toBe(0)
    })
})
const findRomanNumber = require('./findRomanNumber')

describe('Test findRomanNumber',()=>{
    it('Should return V when number is 5 input',()=>{
        expect(findRomanNumber(5)).toBe("V")
    })
    it('Should return CM when number is 900 input',()=>{
        expect(findRomanNumber(900)).toBe("CM")
    })
    it('Should return CC when number is 200 input',()=>{
        expect(findRomanNumber(200)).toBe("CC")
    })
})
    